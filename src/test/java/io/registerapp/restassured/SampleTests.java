//package io.registerapp.restassured;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import io.registerapp.controllers.UserOperations;
//import io.registerapp.controllers.UserRoles;
//import io.restassured.response.Response;
//import io.restassured.specification.RequestSpecification;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class SampleTests 
//{
//	 private static final Logger logger = LoggerFactory.getLogger(SampleTests.class);
//	 
//	 RequestSpecification request;
//	 Response response;
//	 
//
//	 
//	 @MockBean
//	 private UserRoles roleService;
//
//
//	 @Autowired
//	 private MockMvc mvc;
//
//	 
//	 
//	 @InjectMocks
//	 private UserOperations userOpController = new UserOperations();
//
//	 
//	 @Before
//	    public void init() {
//			 Mockito.when(roleService.isAdmin("alex"))
//	         		.thenReturn(false);
//	    }
//	 
//
//	 
//	 //This test deletes the 'alex' user receiving a negative mocked response on the "isAdmin()" call
//	 //@Test
////	 public void deleteAdminUserUsingMockValidationService() throws Exception
////	 {
////		 //final UserRolesService userRolesMock = Mockito.spy(new UserRolesService());
////
////		 this.mvc.perform(MockMvcRequestBuilders
////		            .delete("/user/usemock/{username}", "alex")
////		            .contentType(MediaType.APPLICATION_JSON)
////		            .accept(MediaType.APPLICATION_JSON))
////	                .andExpect(status().is(204));
////		            
//		 //userOpController.deleteUserUsingMock("alex");
//	 //}
//	 
//	 
////	 @DataProvider(name = "userToDeleteList")
////	 public Object[][] dataProviderMethod() {
////	    return new Object[][] { { "alex" }, { "nonalex" } };
////	 }
////	 
////	 @Test(dataProvider = "userToDeleteList")
////	 public void should_returnForbidden_when_deletingAdminUser(String usernameToDelete) throws Exception 
////	 {
////		//check if user exists - before attempting to delete
////		response = request.get("/user/" + usernameToDelete);
////		assertThat(response.body().asString()).isNotEmpty();
////		
////		response = request.delete("/user/usemock/" + usernameToDelete);
////
////		//assert by status code
////		assertThat(response.statusCode()).isEqualTo(HttpStatus.FORBIDDEN_403);
////		
////		//assert by entry/data check
////		//check if user exists - after attempting to delete
////		response = request.get("/user/" + usernameToDelete);
////		assertThat(response.body().asString()).isNotEmpty();
////		
////		logger.info("Succesfully executed test: {should_returnForbidden_when_deletingAdminUser} for dataSet: {" + usernameToDelete + "}" );
////	 }
////	 
////	 @Test(dataProvider = "userToDeleteList")
////	 public void should_returnForbidden_when_deletingAdminUser_v2(String usernameToDelete) throws Exception 
////	 {
////		//check if user exists - before attempting to delete
////		response = request.get("/user/" + usernameToDelete);
////		assertThat(response.body().asString()).isNotEmpty();
////		
////		request
////			  .given()
////			  .basePath("user/usemock")
////			  .delete(usernameToDelete)
////			  .then()
////			  .assertThat()
////			  .statusCode(HttpStatus.FORBIDDEN_403);
////					  
////		//assert by status code
////		//assertThat(response.statusCode()).isEqualTo(HttpStatus.FORBIDDEN_403);
////		
////		//assert by entry/data check
////		//check if user exists - after attempting to delete
////		response = request.get("/user/" + usernameToDelete);
////		assertThat(response.body().asString()).isNotEmpty();
////		
////		logger.info("Succesfully executed test: {should_returnForbidden_when_deletingAdminUser_v2} for dataSet: {" + usernameToDelete + "}" );
////	 }
//}
