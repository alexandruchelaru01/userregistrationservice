package io.registerapp.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.registerapp.models.User;

public interface UserRepository extends MongoRepository<User, Integer> {
}

