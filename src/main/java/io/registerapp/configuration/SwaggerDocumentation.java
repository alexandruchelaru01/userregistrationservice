package io.registerapp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;


@Configuration
public class SwaggerDocumentation {
	
    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("User Registration Service")
            .description("This spec describes the user registration services")      
            .version("1.0.0")
            .contact(new Contact("","", "apiteam@swagger.io"))  
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("io.registerapp.controllers"))
                    .build()
                .directModelSubstitute(org.threeten.bp.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(org.threeten.bp.OffsetDateTime.class, java.util.Date.class)
                .apiInfo(apiInfo());
    }

//    private ApiKey apiKey() {
//      return new ApiKey("mykey", "api_key", "header"); 
//    }
//
//    private SecurityContext securityContext() {
//      return SecurityContext.builder()
//          .securityReferences(defaultAuth())
//          .forPaths(PathSelectors.regex("/anyPath.*")) 
//          .build();
//    }
//
//    List<SecurityReference> defaultAuth() {
//      AuthorizationScope authorizationScope
//          = new AuthorizationScope("global", "accessEverything");
//      AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//      authorizationScopes[0] = authorizationScope;
//      return Collections.singletonList(
//          new SecurityReference("mykey", authorizationScopes)); 
//    }
//
//    @Bean
//    SecurityConfiguration security() {
//      return SecurityConfigurationBuilder.builder() 
//          .clientId("test-app-client-id")
//          .clientSecret("test-app-client-secret")
//          .realm("test-app-realm")
//          .appName("test-app")
//          .scopeSeparator(",")
//          .additionalQueryStringParams(null)
//          .useBasicAuthenticationWithAccessCodeGrant(false)
//          .build();
//    }

    @Bean
    UiConfiguration uiConfig() {
      return UiConfigurationBuilder.builder() 
          .deepLinking(true)
          .displayOperationId(false)
          .defaultModelsExpandDepth(1)
          .defaultModelExpandDepth(1)
          .defaultModelRendering(ModelRendering.EXAMPLE)
          .displayRequestDuration(true)
          .docExpansion(DocExpansion.NONE)
          .filter(false)
          .maxDisplayedTags(null)
          .operationsSorter(OperationsSorter.ALPHA)
          .showExtensions(false)
          .tagsSorter(TagsSorter.ALPHA)
          .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
          .validatorUrl(null)
          .build();
    }

}
