package io.registerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//Application opens on Swagger url: http://localhost:8081/v2/swagger-ui.html
//Port can be configured in application.properties file in scr/main/resources

@SpringBootApplication
@EnableSwagger2
public class Main {

    public static void main(String[] args) throws Exception {
        new SpringApplication(Main.class).run(args);
    }

}
