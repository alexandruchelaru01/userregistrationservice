package io.registerapp.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.registerapp.models.User;
import io.registerapp.repositories.UserRepository;

@RestController
@RequestMapping("/users")
public class UserRegistration {
    private UserRepository userRepository;

    public UserRegistration(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/getAll")
    public List<User> getAll() {
        return userRepository.findAll();
    }
    
    @PostMapping(path = "/addUser", consumes = "application/json", produces = "application/json")
    public void addUser(@Valid @RequestBody User user) {
    	userRepository.insert(user);
    }
    
    @DeleteMapping(path = "/deleteUserById")
    public void deleteUser(int id)
    {
    	userRepository.delete(id);
    }
    
    @DeleteMapping(path = "/deleteAll")
    public void deleteAll()
    {
    	userRepository.deleteAll();
    }
    
    @PutMapping(path = "/updateUser")
    public void updateUser(@Valid @RequestBody User user)
    {
    	userRepository.save(user);
    }
}
