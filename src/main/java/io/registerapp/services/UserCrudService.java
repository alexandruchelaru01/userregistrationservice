//package io.registerapp.services;
//
//import java.io.IOException;
//import org.bson.Document;
//import org.bson.conversions.Bson;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import com.fasterxml.jackson.core.JsonParseException;
//import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.gson.Gson;
//import com.mongodb.BasicDBObject;
//import com.mongodb.client.MongoCollection;
//import com.mongodb.client.MongoDatabase;
//import io.registerapp.database.DBConnect;
//import io.registerapp.database.DBUtils;
//import io.registerapp.models.User;
//
//@Service
//public class UserCrudService {
//    private final MongoDatabase database;
//    private final DBConnect dbObj;
//    
//    @Autowired
//    private ObjectMapper objectMapper;
//    
//    public UserCrudService() {	
//        dbObj = new DBConnect();
//        database = dbObj.getDB("test");
//    }
//
//    
//	//checks if a user is present in db
//	public boolean isUserExist(String username)
//	{
//            //Search user in db 
//            MongoCollection<Document> collection = database.getCollection("users");
//            Document doc = DBUtils.getRecordByPropertyValue(collection, "username", username).first();
//    	
//            return doc != null;
//	}
//	
//	//returns user by username
//	public User getUserByUsername(String username) throws JsonParseException, JsonMappingException, IOException
//	{
//            //Search user in db 
//            MongoCollection<Document> collection = database.getCollection("users");
//            Document doc = DBUtils.getRecordByPropertyValue(collection, "username", username).first();
//
//            return objectMapper.readValue(doc.toJson(), User.class);
//	}
//	
//	public void createUser(User user)
//	{
//            // Deserialize object to json string
//            Gson gson = new Gson();
//            String json = gson.toJson(user);
//
//            MongoCollection<Document> collection = database.getCollection("users");
//            // Parse to bson document and insert
//            Document doc = Document.parse(json);
//
//            collection.insertOne(doc);
//	}
//	
//	//deteles a user by username
//	public void deleteUserByUsername(String username)
//	{
//            MongoCollection<Document> collection = database.getCollection("users");
//
//            Bson condition = new Document("$eq", username);
//            Bson filter = new Document("username", condition);   	
//
//            collection.deleteOne(filter);  
//	}
//	
//	//updates a user by username
//	public void updateUserByUsername(String username, User body)
//	{
//            MongoCollection<Document> collection = database.getCollection("users"); 	
//
//            //setup filter to retrieve desired user
//            Bson condition = new Document("$eq", username);
//            Bson filter = new Document("username", condition);
//
//            //Deserialize object from body to document
//            // Deserialize object to json string
//            Gson gson = new Gson();
//            String json = gson.toJson(body);
//            Document newDoc = Document.parse(json);
//
//            BasicDBObject updateObject = new BasicDBObject();
//            updateObject.put("$set", newDoc); // (3)
//
//            collection.updateOne(filter, updateObject);
//	}
//        
//        //Remove documents from collection
//        public void clearCollection()
//        {
//            MongoCollection<Document> collection = database.getCollection("users");
//            
//            BasicDBObject document = new BasicDBObject();
//            
//            // Delete All documents from collection Using blank BasicDBObject
//            collection.deleteMany(document);
//        }
//}
