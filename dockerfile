#!/bin/bash

FROM java:8-jdk-alpine

COPY ./target/registrationservice-1.0.0.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch registrationservice-1.0.0.jar'

CMD ["java","-jar","registrationservice-1.0.0.jar"]
EXPOSE 6062
EXPOSE 6063
EXPOSE 6064
 

